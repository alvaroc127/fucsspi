package fucsspi.factory;


import org.jboss.logging.Logger;
import org.keycloak.Config.Scope;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.UserModel;
import org.keycloak.storage.UserStorageProviderFactory;

import fucsspi.provider.ImpUsuarioProvider;

public class ImpUsuarioFactoryProvider implements UserStorageProviderFactory<ImpUsuarioProvider>
{

	private static final Logger logger =
			Logger.getLogger(ImpUsuarioFactoryProvider.class);


	@Override
	public ImpUsuarioProvider create(KeycloakSession session, ComponentModel model) {
		return new ImpUsuarioProvider(session, model);
	}

	
	@Override
	public String getId() 
	{
		return "fucs-conect";
	}
	
	
	
	
	
	

}
