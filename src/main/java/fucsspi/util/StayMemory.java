package fucsspi.util;

import java.util.Hashtable;

import org.keycloak.representations.idm.RolesRepresentation;

import fucsspi.model.Permiso;
import fucsspi.model.Rol;
import fucsspi.model.TipoDocumento;

public class StayMemory {
	
	
	public static Hashtable<Integer,Rol> ROLES=new Hashtable<>();
	public static Hashtable<Integer,TipoDocumento> TIPODOCS=new Hashtable<>();
	public static Hashtable<Integer,Permiso> PERMISOS=new Hashtable<>();
	
	
	
	
	public static boolean noLoad() 
	{
		boolean a=null == ROLES || ROLES.isEmpty();
		boolean b=null == TIPODOCS || TIPODOCS.isEmpty();
		boolean c=null == PERMISOS || PERMISOS.isEmpty();
		return a && b && c;
	}
	
	public static void loadInMemoryPERMISOS(Integer key, Permiso perm) 
	{
		PERMISOS.put(key, perm);
	}
	
	public static void loadInMemoryROLES(Integer key, Rol rol) 
	{
		ROLES.put(key,rol);
		
	}
	
	public static void loadInMemoryTIPODOCS(Integer key,TipoDocumento tip) 
	{
		TIPODOCS.put(key, tip);
	}
	
	
	

}
