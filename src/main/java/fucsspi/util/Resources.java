package fucsspi.util;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;


import org.jboss.logging.Logger;

import fucsspi.provider.ImpUsuarioProvider;

public class Resources 
{
	
	private static final Logger logger =
			Logger.getLogger(ImpUsuarioProvider.class);
	private HashMap<String,Properties> properties;
	
	
	public Resources() {
		logger.debug("inicio de las carga ");
		this.properties=new HashMap<String,Properties>();
		init();
		// TODO Auto-generated constructor stub
	}
	
		
	public HashMap<String, Properties> getProperties() {
		return properties;
	}




	public void setProperties(HashMap<String, Properties> properties) {
		this.properties = properties;
	}


	public String getProperti(String propertyKey,String key) 
	{
		Properties pro=null;
		if(null!=this.properties.get(propertyKey)) 
		{
			pro=(Properties)this.properties.get(propertyKey);
			return pro.getProperty(key);
		}
		return null;
	}
	
	public void init() 
	{
		try {
			logger.debug("inicio de las carga del interno ");
		ResourceBundle bund=ResourceBundle.getBundle(Constantes.NAMEPROINTER);
		Enumeration<String> keys=bund.getKeys();
		while(keys.hasMoreElements()) 
			{
			  String key=keys.nextElement();
			  String value=bund.getString(key);
			  if(null!=value) {
			  FileInputStream fileInput=new FileInputStream(new java.io.File(value));
			  Properties prop=new Properties();
			  prop.load(fileInput);
			  this.properties.put(key,prop);
			  }
			}
		logger.debug("properties cargados");
		}catch 
		(Exception e) 
		{
			logger.debug("error en la carga de properties  ",e);
			System.out.println("se genero un error en obteniendo las propiedades");
			e.printStackTrace();
		}
		
		
	}




}
