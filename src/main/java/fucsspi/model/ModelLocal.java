package fucsspi.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.ClientModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.StorageId;

public class ModelLocal implements UserModel 
{
	
	protected KeycloakSession session;
    protected RealmModel realm;
    protected ComponentModel storageProviderModel;
    
    private String username;
    protected StorageId storageId;
    private String email;
    private String firstName;
    private String LastName;
    private java.util.Calendar datecreate;
    private Integer status;
    private Usuario us;
    
    
    
    public ModelLocal   (KeycloakSession session, RealmModel realm, ComponentModel storageProviderModel,Usuario usu) {
        this.session = session;
        this.realm = realm;
        this.storageProviderModel = storageProviderModel;
        this.username=usu.getName();
        this.email=usu.getEmail();
        this.datecreate=usu.getDateCreated();
        this.status=usu.getStatus();
        this.us=usu;
        /*if(null!=usu.getRoles() &&!usu.getRoles().isEmpty()) 
        {
        	for(Integer rol: usu.getRoles().keySet()) 
        	{
        		Rol role1=usu.getRoles().get(rol);
        		RoleModel rolm=this.realm.getRole(role1.getDescrip());
        		if(null==rolm) 
        		{
        			this.realm.addRole(role1.getDescrip());
        		}
        	}
        }*/
    }


	@Override
	public Set<RoleModel> getRealmRoleMappings() 
	{
		/*Set<RoleModel> setout=null;
		for(Integer key1:this.us.getRoles().keySet()) 
		{
			Rol rol=this.us.getRoles().get(key1);
			setout=realm.getRolesStream().filter( rm -> rm.getName().equals(rol.getDescrip())).collect(Collectors.toSet());
		}*/
		return null;
	}

	@Override
	public Set<RoleModel> getClientRoleMappings(ClientModel app) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasRole(RoleModel role) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void grantRole(RoleModel role) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<RoleModel> getRoleMappings() {
		// TODO Auto-generated method stub
		  Set<RoleModel> set = new HashSet<>();
	        if (appendDefaultRolesToRoleMappings()) set.addAll(realm.getDefaultRole().getCompositesStream().collect(Collectors.toSet()));
	        set.addAll(getRoleMappingsInternal());
	        return set;
	}
	
	  protected Set<RoleModel> getRoleMappingsInternal() {
	        return Collections.emptySet();
	    }
	
	 protected boolean appendDefaultRolesToRoleMappings() {
	        return true;
	    }

	@Override
	public void deleteRoleMapping(RoleModel role) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getId() {
		if (storageId == null) {
            storageId = new StorageId(storageProviderModel.getId(), getUsername());
        }
        return storageId.getId();
	}

	@Override
	public String getUsername() 
	{
		return this.username;
	}

	@Override
	public void setUsername(String username) {
		this.username=username;

	}

	@Override
	public Long getCreatedTimestamp() {
		// TODO Auto-generated method stub
		return this.datecreate.getTimeInMillis();
	}

	@Override
	public void setCreatedTimestamp(Long timestamp) 
	{
		this.datecreate.setTimeInMillis(timestamp);
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return this.status==1?true:false;
	}

	@Override
	public void setEnabled(boolean enabled) 
	{

	}

	@Override
	public void setSingleAttribute(String name, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setAttribute(String name, List<String> values) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeAttribute(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getFirstAttribute(String name) {
		 if (name.equals(UserModel.USERNAME)) {
	            return getUsername();
	        }
		 return null;
	}

	@Override
	public List<String> getAttribute(String name) {
		 if (name.equals(UserModel.USERNAME)) {
	            return Collections.singletonList(getUsername());
	        }
	        return Collections.emptyList();
	}

	@Override
	public Map<String, List<String>> getAttributes() {
		 MultivaluedHashMap<String, String> attributes = new MultivaluedHashMap<>();
	        attributes.add(UserModel.USERNAME, getUsername());
	        return attributes;
	}

	@Override
	public Set<String> getRequiredActions() {
		return Collections.emptySet();
	}

	@Override
	public void addRequiredAction(String action) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeRequiredAction(String action) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getFirstName() {
		// TODO Auto-generated method stub
		return this.firstName;
	}

	@Override
	public void setFirstName(String firstName) 
	{
		this.firstName=firstName;

	}

	@Override
	public String getLastName() {
		return this.LastName;
	}

	@Override
	public void setLastName(String lastName) 
	{
		this.LastName=lastName;
	}

	@Override
	public String getEmail() 
	{
		return this.email;
	}

	@Override
	public void setEmail(String email) 
	{
		this.email=email;

	}

	@Override
	public boolean isEmailVerified() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setEmailVerified(boolean verified) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<GroupModel> getGroups() {
		return this.getGroupsStream().collect(Collectors.toSet());
	}
	
	
	@Override
    public Stream<GroupModel> getGroupsStream() {
        Stream<GroupModel> groups = getGroupsInternal().stream();
        if (appendDefaultGroups()) groups = Stream.concat(groups, realm.getDefaultGroupsStream());
        return groups;
    }
	
	  protected Set<GroupModel> getGroupsInternal() {
	        return Collections.emptySet();
	    }

	
	protected boolean appendDefaultGroups() {
        return true;
    }

	@Override
	public void joinGroup(GroupModel group) {
		// TODO Auto-generated method stub

	}

	@Override
	public void leaveGroup(GroupModel group) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isMemberOf(GroupModel group) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getFederationLink() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFederationLink(String link) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getServiceAccountClientLink() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setServiceAccountClientLink(String clientInternalId) {
		// TODO Auto-generated method stub

	}

}
