package fucsspi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jboss.logging.Logger;


import fucsspi.model.Rol;
import fucsspi.util.StayMemory;

public class DAORol extends BaseDao
{
	
	private final static String SELECTALLROLL ="SELECT ID_ROL,STATUS,NOMBRE FROM dbo.ROL";
	
	private static final Logger logger =
			Logger.getLogger(DAORol.class);
	 
	
	public DAORol() {
		super();
	// TODO Auto-generated constructor stub
	}
	
	
	
	public void findallRoll()
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		ResultSet rs=null;
		try 
		{
		 stat=con.prepareStatement(this.SELECTALLROLL);
		 rs=stat.executeQuery();
		 	while(rs.next()) 
		 	{
		 		Rol rol=new Rol();
		 		rol.setId(rs.getInt("ID_ROL"));
		 		rol.setEstado(rs.getInt("STATUS"));
		 		rol.setDescrip(rs.getString("NOMBRE"));
		 		StayMemory.loadInMemoryROLES(rol.getId(),rol);
		 	}	
		}catch (Exception e) 
		{
			logger.debug("error en la conexion",e);
		e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);
	}
	
	
	

}
