package fucsspi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jboss.logging.Logger;

import fucsspi.model.Permiso;
import fucsspi.util.StayMemory;

public class DAOPermiso extends BaseDao
{
	
	private final static String SELECTALLPERMI ="SELECT ID_PER,STATUS,NOMBRE FROM dbo.PERMISOS";
	
	private static final Logger logger =
			Logger.getLogger(DAOPermiso.class);
	 
	 
	
	public DAOPermiso() {
		super();
	// TODO Auto-generated constructor stub
	}
	
	
	
	public void findPermis()
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		ResultSet rs=null;
		try 
		{
		 stat=con.prepareStatement(this.SELECTALLPERMI);
		 rs=stat.executeQuery();
		 	while(rs.next()) 
		 	{
		 		Permiso per=new Permiso();
		 		per.setId(rs.getInt("ID_PER"));
		 		per.setEstado(rs.getInt("STATUS"));
		 		per.setDescrip(rs.getString("NOMBRE"));
		 		StayMemory.loadInMemoryPERMISOS(per.getId(),per);
		 	}
		}catch (Exception e) 
		{
		logger.debug("error en la conexion",e);
		e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);
	}
	
	
	

}
