package fucsspi.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;


import org.jboss.logging.Logger;

import fucsspi.model.Permiso;
import fucsspi.model.Rol;
import fucsspi.model.TipoDocumento;
import fucsspi.model.Usuario;
import fucsspi.util.Constantes;
import fucsspi.util.StayMemory;

public class DAOUsuario extends BaseDao
{
	private static final Logger logger =
			Logger.getLogger(DAOTipoDocumento.class);
	private final static String SELECTALLUSUARI=
			" SELECT  us.ID_USU,us.NUMERO_DOC ,us.DATE_CREATE ,us.EMAIL ,us.EMAIL, " +
			" us.TIPO_DOC, us.password ,us.STATUS ,us.DATE_CREATE, " +
			" uxr.ID_ROL AS ROLCODE, rxp.ID_PER AS PERCODE, us.date_update " +
			" FROM  dbo.USUARIOXROL uxr " +
			" INNER JOIN dbo.USUARIO us ON (uxr.ID_USU = us.ID_USU ) " +
			" INNER JOIN dbo.ROLXPERMISOS rxp  ON (rxp.ID_ROL = uxr.ID_ROL ) " +
			" INNER JOIN dbo.ROL r ON (r.ID_ROL = rxp.ID_ROL ) " +
			" INNER JOIN dbo.PERMISOS p  ON (rxp.ID_PER =p.ID_PER ) " +
			" WHERE  us.STATUS = 1 AND r.STATUS  = 1 AND p.STATUS  = 1 ";
	
	private  static final String SELECTBYDOTIP=
			" SELECT  us.ID_USU,us.NUMERO_DOC ,us.DATE_CREATE ,us.EMAIL ,us.EMAIL, " +
			" us.TIPO_DOC, us.password ,us.STATUS ,us.DATE_CREATE, " +
			" uxr.ID_ROL AS ROLCODE, rxp.ID_PER AS PERCODE, us.date_update " +
			" FROM  dbo.USUARIOXROL uxr " +
			" INNER JOIN dbo.USUARIO us ON (uxr.ID_USU = us.ID_USU ) " +
			" INNER JOIN dbo.ROLXPERMISOS rxp  ON (rxp.ID_ROL = uxr.ID_ROL ) " +
			" INNER JOIN dbo.ROL r ON (r.ID_ROL = rxp.ID_ROL ) " +
			" INNER JOIN dbo.PERMISOS p  ON (rxp.ID_PER =p.ID_PER ) " +
			" WHERE  us.STATUS = 1 AND r.STATUS  = 1 AND p.STATUS  = 1 " +
			 " AND us.TIPO_DOC = ?" +
			 " AND us.NUMERO_DOC = ?";
	
	private  static final String SELECTBYEMAIL= 
			" SELECT  us.ID_USU,us.NUMERO_DOC ,us.DATE_CREATE ,us.EMAIL ,us.EMAIL, " +
			" us.TIPO_DOC, us.password ,us.STATUS ,us.DATE_CREATE, " +
			" uxr.ID_ROL AS ROLCODE, rxp.ID_PER AS PERCODE, us.date_update " +
			" FROM  dbo.USUARIOXROL uxr " +
			" INNER JOIN dbo.USUARIO us ON (uxr.ID_USU = us.ID_USU ) " +
			" INNER JOIN dbo.ROLXPERMISOS rxp  ON (rxp.ID_ROL = uxr.ID_ROL ) " +
			" INNER JOIN dbo.ROL r ON (r.ID_ROL = rxp.ID_ROL ) " +
			" INNER JOIN dbo.PERMISOS p  ON (rxp.ID_PER =p.ID_PER ) " +
			" WHERE  us.STATUS = 1 AND r.STATUS  = 1 AND p.STATUS  = 1 " +
			" and us.EMAIL = ?" +
			" AND us.STATUS = 1";
	
	
	
	private final static String ADDUSER=
			" INSERT INTO FUCS.dbo.USUARIO (TIPO_DOC,EMAIL, NUMERO_DOC,STATUS) "+
			 " VALUES (?,?,?,1) ";
	
	
	
	private final static String ADDUSEREMAIL=
			" INSERT INTO FUCS.dbo.USUARIO (EMAIL,STATUS) "+
			 " VALUES (?,1) ";
	
	private final static String ADDUSEROL=
			"INSERT INTO FUCS.dbo.USUARIOXROL (ID_ROL,ID_USU) VALUES (?,?)";
	
	
	
	private final static String UPDATEUSERDEL=
			" UPDATE dbo.USUARIO "+
			" SET STATUS = ? " +
			" WHERE NUMERO_DOC = ? " +
			" AND TIPO_DOC  = ? ";
	

	
	private  static final String SELECTVALIDPASS=
			 " SELECT 1 "+
			 " FROM dbo.USUARIO "+
			 " WHERE TIPO_DOC = ?" +
			 " AND NUMERO_DOC = ?" +
			 " AND password = ?";
	
	private static final String UPDATEPASSWORD=
			" UPDATE dbo.USUARIO " +
			" SET password = ? " +
			" WHERE NUMERO_DOC = ? " +
			" AND TIPO_DOC  = ? ";
	
	
	 
	 
	
	public DAOUsuario() {
		super();
		logger.debug("Inicio del dao USUARIO");
	// TODO Auto-generated constructor stub
	}
	
	
	
	public java.util.Hashtable<String, Usuario> findallUser()
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		ResultSet rs=null;
		Usuario us=null;
		java.util.Hashtable<String, Usuario> usuarios=new java.util.Hashtable<String, Usuario>();
		logger.debug("Inicio dEL FINDALLUSUARIO");
		try 
		{
		 stat=con.prepareStatement(this.SELECTALLUSUARI);
		 rs=stat.executeQuery();
		 	while(rs.next()) 
		 	{
		 		if (!usuarios.isEmpty()) us=(Usuario)usuarios.get(rs.getInt("TIPO_DOC")+rs.getString("NUMERO_DOC"));
		 		if(null==us) 
		 		{
		 		us=new Usuario();
		 		us.setIdusu(rs.getLong("ID_USU"));
		 		
		 		us.setEmail(rs.getString("EMAIL"));
		 		us.setNumerodoc(rs.getString("NUMERO_DOC"));
		 		//us.setGenero(rs.getInt("GENERO"));
		 		//rs.getTimestamp("FECHA_NACIMIENTO");
		 		//us.setDireccion(rs.getString("DIRECCION"));
		 		//us.setNumeroCel(rs.getString("NUMEROCEL"));
		 		us.setPassword(rs.getString("password"));
		 		us.setStatus(rs.getInt("STATUS"));
		 		Calendar cal=Calendar.getInstance();
		 		cal.setTimeInMillis(rs.
		 				getTimestamp("DATE_CREATE").getTime());
		 		us.setDateCreated(cal);
		 		us.setName(rs.getInt("TIPO_DOC")+us.getNumerodoc());
		 		Rol rol=StayMemory.ROLES.get(rs.getInt("ROLCODE"));
		 		Permiso perm=StayMemory.PERMISOS.get(rs.getInt("PERCODE"));
		 		rol.addPermis(perm.getId(),perm);
		 		us.usuarioaddRol(rol.getId(), rol);
		 		usuarios.put(us.getName(),us);
		 		}
		 		else
		 		{
		 			Rol rol=StayMemory.ROLES.get(rs.getInt("ROLCODE"));
			 		Permiso perm=StayMemory.PERMISOS.get(rs.getInt("PERCODE"));
			 		rol.addPermis(perm.getId(),perm);
			 		us.usuarioaddRol(rol.getId(), rol);
			 		usuarios.put(us.getName(),us);
		 		}
		 	}	
		}catch (Exception e) 
		{
			logger.debug("error del dao USUARIO",e);
		e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);
		return usuarios;
	}
	
	
	/*public java.util.List<Usuario> findallUserList()
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		ResultSet rs=null;
		java.util.List<Usuario> usuarios=new java.util.ArrayList<Usuario>();
		try 
		{
			logger.debug("Inicio dEL FINDALLUSUARIO");
		 stat=con.prepareStatement(this.SELECTALLUSUARI);
		 rs=stat.executeQuery();
		 logger.debug("Inicio OBTIENE CONEXION");
		 	while(rs.next()) 
		 	{
		 		Usuario us=new Usuario();
		 		us.setIdusu(rs.getLong("ID_USU"));
		 		
		 		us.setEmail(rs.getString("EMAIL"));
		 		us.setNumerodoc(rs.getString("NUMERO_DOC"));
		 		us.setGenero(rs.getInt("GENERO"));
		 		//rs.getTimestamp("FECHA_NACIMIENTO");
		 		us.setDireccion(rs.getString("DIRECCION"));
		 		us.setNumeroCel(rs.getString("NUMEROCEL"));
		 		us.setPassword(rs.getString("password"));
		 		us.setStatus(rs.getInt("STATUS"));
		 		Calendar cal=Calendar.getInstance();
		 		cal.setTimeInMillis(rs.
		 				getTimestamp("DATE_CREATE").getTime());
		 		us.setDateCreated(cal);
		 		us.setName(rs.getInt("TIPO_DOC")+us.getNumerodoc());
		 		usuarios.add(us);
		 	}	
		}catch (Exception e) 
		{
			logger.debug("error del dao USUARIO",e);
		e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);
		return usuarios;
	}*/
	
	
	
	public Usuario findbyEmail(String email) 
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		ResultSet rs=null;
		Usuario us=null;
		java.util.Hashtable<String, Usuario> usuarios=new java.util.Hashtable<String, Usuario>();
		try
		{
			 stat=con.prepareStatement(this.SELECTBYEMAIL);
			 stat.setString(1,email);
			 rs=stat.executeQuery();
			while(rs.next()) 
		 	{
				if (!usuarios.isEmpty()) us=(Usuario)usuarios.get(rs.getInt("TIPO_DOC")+rs.getString("NUMERO_DOC"));
				if(null==us) 
				{
					us.setIdusu(rs.getLong("ID_USU"));
			 		us.setEmail(rs.getString("EMAIL"));
			 		us.setNumerodoc(rs.getString("NUMERO_DOC"));
			 		//us.setGenero(rs.getInt("GENERO"));
			 		//rs.getTimestamp("FECHA_NACIMIENTO");
			 		//us.setDireccion(rs.getString("DIRECCION"));
			 		//us.setNumeroCel(rs.getString("NUMEROCEL"));
			 		us.setPassword(rs.getString("password"));
			 		us.setStatus(rs.getInt("STATUS"));
			 		Calendar cal=Calendar.getInstance();
			 		cal.setTimeInMillis(rs.
			 				getTimestamp("DATE_CREATE").getTime());
			 		us.setDateCreated(cal);
			 		us.setName(rs.getInt("TIPO_DOC")+us.getNumerodoc());
			 		Rol rol=StayMemory.ROLES.get(rs.getInt("ROLCODE"));
			 		Permiso perm=StayMemory.PERMISOS.get(rs.getInt("PERCODE"));
			 		rol.addPermis(perm.getId(),perm);
			 		us.usuarioaddRol(rol.getId(), rol);
			 		usuarios.put(us.getName(),us);
					
				}else 
				{	
					Rol rol=StayMemory.ROLES.get(rs.getInt("ROLCODE"));
			 		Permiso perm=StayMemory.PERMISOS.get(rs.getInt("PERCODE"));
			 		rol.addPermis(perm.getId(),perm);
			 		us.usuarioaddRol(rol.getId(), rol);
			 		usuarios.put(us.getName(),us);
				}
		 	}	
		}catch (Exception e) 
		{
		logger.debug("error del dao USUARIO",e);
		 e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);
		return us;
	}
	
	
	public Usuario findbyDocNumero(Integer tipodoc,String numeroDoc) 
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		ResultSet rs=null;
		Usuario us=null;
		java.util.Hashtable<String, Usuario> usuarios=new java.util.Hashtable<String, Usuario>();
		try
		{
			 stat=con.prepareStatement(this.SELECTBYDOTIP);
			 stat.setInt(1,tipodoc);
			 stat.setString(2,numeroDoc);
			 rs=stat.executeQuery();
			while(rs.next()) 
		 	{
				if (!usuarios.isEmpty()) us=(Usuario)usuarios.get(rs.getInt("TIPO_DOC")+rs.getString("NUMERO_DOC"));
		 		if(null==us) 
		 		{
		 		us=new Usuario();
		 		us.setIdusu(rs.getLong("ID_USU"));
		 		
		 		us.setEmail(rs.getString("EMAIL"));
		 		us.setNumerodoc(rs.getString("NUMERO_DOC"));
		 		//us.setGenero(rs.getInt("GENERO"));
		 		//rs.getTimestamp("FECHA_NACIMIENTO");
		 		//us.setDireccion(rs.getString("DIRECCION"));
		 		//us.setNumeroCel(rs.getString("NUMEROCEL"));
		 		us.setPassword(rs.getString("password"));
		 		us.setStatus(rs.getInt("STATUS"));
		 		Calendar cal=Calendar.getInstance();
		 		cal.setTimeInMillis(rs.
		 				getTimestamp("DATE_CREATE").getTime());
		 		us.setDateCreated(cal);
		 		us.setName(rs.getInt("TIPO_DOC")+us.getNumerodoc());
		 		Rol rol=StayMemory.ROLES.get(rs.getInt("ROLCODE"));
		 		Permiso perm=StayMemory.PERMISOS.get(rs.getInt("PERCODE"));
		 		rol.addPermis(perm.getId(),perm);
		 		us.usuarioaddRol(rol.getId(), rol);
		 		usuarios.put(us.getName(),us);
		 		}
		 		else
		 		{
		 			Rol rol=StayMemory.ROLES.get(rs.getInt("ROLCODE"));
			 		Permiso perm=StayMemory.PERMISOS.get(rs.getInt("PERCODE"));
			 		rol.addPermis(perm.getId(),perm);
			 		us.usuarioaddRol(rol.getId(), rol);
			 		usuarios.put(us.getName(),us);
		 		}
		 	}	
		}catch (Exception e) 
		{
			logger.debug("error del dao USUARIO",e);
		 e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);
		return us;
	}
	
	
	
	public void insertUser(Integer tipo,Usuario us) 
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		try
		{
			 stat=con.prepareStatement(this.ADDUSER);
			 stat.setInt(1,tipo);
			 stat.setString(2,us.getEmail());
			 stat.setString(3,us.getNumerodoc());
			 stat.executeUpdate();
		}catch (Exception e) 
		{
			logger.debug("un error insertando el usuario",e);
			e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	}
	
	
	public void insertUserEmail(Usuario us) 
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		try
		{
			 stat=con.prepareStatement(this.ADDUSEREMAIL);
			 stat.setString(1,us.getEmail());
			 stat.executeUpdate();
		}catch (Exception e) 
		{
			logger.debug("un error insertando el usuario",e);
			e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	}
	
	
	
	public int deleteLogicalUpdate(Integer tipodoc,Usuario us) 
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		Integer out=-1;
		try 
		{
			stat=con.prepareStatement(this.UPDATEUSERDEL);
			stat.setInt(1,Constantes.offUser);
			stat.setString(2, us.getNumerodoc());
			stat.setInt(3,tipodoc);
			out= stat.executeUpdate();			
		}catch (Exception e) 
		{
			logger.debug("un error en elminarupate el usuario",e);
			e.printStackTrace();	
			out= -1;
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	return out;
	}
	
	public boolean validPassword(Integer tipodoc,Usuario us) 
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		ResultSet rs=null;
		boolean outflag=false;
		Integer out=0;
		try 
		{
			logger.debug("inicio de la consulta");
			logger.debug(us.getNumerodoc());
			logger.debug(us.getPassword());
			logger.debug(tipodoc);
			logger.debug("eso se envia para consultar");
			stat=con.prepareStatement(this.SELECTVALIDPASS);
			stat.setInt(1, tipodoc);
			stat.setString(2, us.getNumerodoc());
			stat.setString(3, us.getPassword());
			rs=stat.executeQuery();
			if(rs.next())
			{
				logger.debug("eso se envia true");
				outflag =true;
			}
			logger.debug("no entro");
		}catch (Exception e)
		{
			logger.debug("ocurrio un error validando el password",e);
			outflag=false;
		}
		logger.debug("no entro");
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);
	 	return outflag;
	}
	
	
	public int updatePassword(Integer tipodoc,Usuario us) 
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		Integer out=0;
		try 
		{
			logger.debug("justo antes de ejecutar el update ");
			stat=con.prepareStatement(this.UPDATEPASSWORD);
			stat.setString(1, us.getPassword());
			stat.setString(2, us.getNumerodoc());
			stat.setInt(3, tipodoc);
			out = stat.executeUpdate();
		}catch (Exception e)
		{
			logger.debug("ocurrio un error validando el password",e);
			out=-1;
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	return out;
	}
	
	
	public int insertRolUseer(Integer tipo,Usuario us) 
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		Integer out=-1;
		try
		{
			 stat=con.prepareStatement(this.ADDUSEROL);
			 stat.setInt(1, us.getRoles().get(Constantes.ROLDEFAULT).getId());
			 stat.setInt(2, new BigDecimal(us.getIdusu()).intValue());
			 out=stat.executeUpdate();
		}catch (Exception e) 
		{
			logger.debug("un error insertando el usuario",e);
			e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	return out;
	}
	
	

}
