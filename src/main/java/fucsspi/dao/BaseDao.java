package fucsspi.dao;

import java.sql.Connection;

import org.jboss.logging.Logger;

import fucsspi.factory.ImpUsuarioFactoryProvider;
import fucsspi.util.Constantes;
import fucsspi.util.Resources;


public class BaseDao {
	
	private static final Logger logger =
			Logger.getLogger(BaseDao.class);
	
	private Conexion con;
	private Resources bund;
	
	public BaseDao() 
	{
		bund=new Resources();
	}
	
	
	public Connection getConnection() 
	{
		logger.debug("inicio de la conexion");
		String jdbcurl=bund.getProperti(Constantes.EXTERNALPROPS,Constantes.JDBCURL);
		String db=bund.getProperti(Constantes.EXTERNALPROPS,Constantes.DB);
		String port=bund.getProperti(Constantes.EXTERNALPROPS,Constantes.PORTDB);
		String hostName=bund.getProperti(Constantes.EXTERNALPROPS,Constantes.SERVERNAME);
		String userdb=bund.getProperti(Constantes.EXTERNALPROPS,Constantes.USERDB);
		String passwd=bund.getProperti(Constantes.EXTERNALPROPS,Constantes.PASSWORD);
		logger.debug("secargaron las propiedades");
		return Conexion.getConnection
		(jdbcurl, userdb, passwd, db, port, hostName);
		
	}


	public Conexion getCon() {
		return con;
	}


	public void setCon(Conexion con) {
		this.con = con;
	}


	public Resources getBund() {
		return bund;
	}


	public void setBund(Resources bund) {
		this.bund = bund;
	}
	
	

}
