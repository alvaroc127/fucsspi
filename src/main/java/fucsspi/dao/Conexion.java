package fucsspi.dao;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import org.jboss.logging.Logger;

public class Conexion 
{
	
	private static final Logger logger =
			Logger.getLogger(Conexion.class);
	private static String URLCONECT="";
	private static String username="";
	private static String password="";
	private static String dataBase="";
	private static String driver="";
	private static Driver drivedt=null;
	





	public Conexion(String uRLCONECT, String username, String password, String dataBase, String driver) {
		super();
		URLCONECT = uRLCONECT;
		this.username = username;
		this.password = password;
		this.dataBase = dataBase;
		this.driver = driver;
	}


	public Conexion() 
	{
	
	}
	
	
	public static Connection getConnection(String uRLCONECT, String username, String password, String dataBase,String portNumber,
			String serverName) 
	{
		Connection con=null;
		logger.debug("tomando objetos de del properties ");		
		try 
		{
			Properties properties=new Properties();
			properties.put("portNumber",portNumber);
			properties.put("serverName", serverName);
			properties.put("user",username);
			properties.put("password",password);
			return DriverManager.getConnection(uRLCONECT,properties);
		}catch (Throwable e) 
		{
			logger.debug("error en la conexion",e);
			e.printStackTrace();
			con=null;
		}
		logger.debug("tomando objetos de del properties ");
		return con;
	}
	
	
	public static void close(PreparedStatement stat) 
	{
		if(null == stat ) {return;}
		try 
		{
			stat.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void close(ResultSet stat) 
	{
		if(null == stat ) {return;}
		try 
		{
			stat.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void close(Connection stat) 
	{
		if(null == stat ) {return;}
		try 
		{
			stat.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
