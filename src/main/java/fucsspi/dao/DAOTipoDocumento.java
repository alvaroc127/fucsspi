package fucsspi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jboss.logging.Logger;


import fucsspi.model.TipoDocumento;
import fucsspi.util.StayMemory;

public class DAOTipoDocumento extends BaseDao
{
	
	private final static String SELECTALLDOC ="SELECT ID_DOC,STATUS,DESCRIP FROM dbo.TIPO_DOC";
	
	private static final Logger logger =
			Logger.getLogger(DAOTipoDocumento.class);
	
	public DAOTipoDocumento() {
		super();
	// TODO Auto-generated constructor stub
	}
	
	
	
	public void findDocumentType()
	{
		Connection con=this.getConnection();
		PreparedStatement stat=null;
		ResultSet rs=null;

		try 
		{
		 stat=con.prepareStatement(this.SELECTALLDOC);
		 rs=stat.executeQuery();
		 	while(rs.next()) 
		 	{
		 		TipoDocumento doc=new TipoDocumento();
		 		doc.setIdtipdoc(rs.getInt("ID_DOC"));
		 		doc.setDescrip(rs.getString("DESCRIP"));
		 		doc.setStatus(rs.getInt("STATUS"));
		 		StayMemory.loadInMemoryTIPODOCS(doc.getIdtipdoc(), doc);
		 	}	
		}catch (Exception e) 
		{
		logger.debug("error en la consulta ",e);
		e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);

	}
	
	
	

}
