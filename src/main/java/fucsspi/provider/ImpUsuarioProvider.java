package fucsspi.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;


import org.jboss.logging.Logger;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputUpdater;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.credential.CredentialModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserCredentialModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.ReadOnlyException;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.adapter.AbstractUserAdapter;
import org.keycloak.storage.user.UserBulkUpdateProvider;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.user.UserQueryProvider;
import org.keycloak.storage.user.UserRegistrationProvider;

import fucsspi.dao.DAOPermiso;
import fucsspi.dao.DAORol;
import fucsspi.dao.DAOTipoDocumento;
import fucsspi.dao.DAOUsuario;
import fucsspi.factory.ImpUsuarioFactoryProvider;
import fucsspi.model.ModelLocal;
import fucsspi.model.Permiso;
import fucsspi.model.Rol;
import fucsspi.model.TipoDocumento;
import fucsspi.model.Usuario;
import fucsspi.util.Constantes;
import fucsspi.util.StayMemory;

public class ImpUsuarioProvider implements UserStorageProvider, 
	UserLookupProvider,
	UserQueryProvider,
	UserRegistrationProvider,
	CredentialInputValidator,
	CredentialInputUpdater
		
{

	
	private static final Logger logger =
			Logger.getLogger(ImpUsuarioProvider.class);
	
	private static final Set<String> disableableTypes = new HashSet<>();

    static 
    {
    	if(StayMemory.noLoad()) 
		{
			DAOPermiso daoper=new DAOPermiso();
			DAORol daorol=new DAORol();
			DAOTipoDocumento daotipo=new DAOTipoDocumento();
			daoper.findPermis();
			daorol.findallRoll();
			daotipo.findDocumentType();
		}
        disableableTypes.add(CredentialModel.PASSWORD);
    }
	
	protected KeycloakSession session;
    
    protected ComponentModel model;
    // map of loaded users in this transaction
    protected Map<String, UserModel> loadedUsers = new java.util.HashMap<>();
	
    
    
	
	public ImpUsuarioProvider(KeycloakSession session, ComponentModel model) {
		super();
		this.session = session;
		this.model = model;
		
		logger.debug("inicio del objeto para consulta");
	}




	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	


	//CredentialInputUpdater
	
	
	  @Override public Set<String> getDisableableCredentialTypes(RealmModel realm,
	  UserModel user) { // TODO Auto-generated method stub
		  logger.debug(" tipos desactivados ");
		  return disableableTypes;
	  }
	  
	  
	  @Override public void disableCredentialType(RealmModel realm, UserModel user,
	  String credentialType) 
	  { 
		  logger.debug(" credential diable ");
		  if(!credentialType.equals(CredentialModel.PASSWORD)) return; 
	  }
	  
	  @Override 
	  public boolean updateCredential(RealmModel realm, UserModel user,CredentialInput input) 
	  {   
		 logger.debug("estoy entrando para actualizar el password");
	  if(!(input instanceof UserCredentialModel)) return false; 
	  if (!input.getType().equals(CredentialModel.PASSWORD)) return false;
	  logger.debug(" pase los dos primeros filtros");
	  UserCredentialModel cred = (UserCredentialModel)input; 
	  Usuario uss=new Usuario(); 
	  String username=user.getUsername();
	  Integer tipdoc=Character.getNumericValue(username.charAt(0)); 
	  String numerodoc=username.substring(1); 
	  DAOUsuario daou=new DAOUsuario();
	  uss.setNumerodoc(numerodoc);
	  uss.setPassword(cred.getValue());
	  logger.debug("el valor ya se configuro");
	  Integer  out=daou.updatePassword(tipdoc,uss);
	  logger.debug("el valor que retorna el update es "+out);
	  boolean outflag=false;
	  outflag=out>0?true:false;
	  if (out>0) 
	  {
		  logger.debug("el del retorno es "+outflag);
		  this.loadedUsers.remove(user.getUsername());
		  this.loadedUsers.put(user.getUsername(),createAdapter(realm, uss, username));
	  }else {
		  logger.debug("el del retorno es "+outflag);
	  }
	  return  outflag;
	  }
	 
	//CredentialInputUpdater

	@Override
	public boolean supportsCredentialType(String credentialType) {
		logger.debug(" credential " + credentialType);
		if (!credentialType.equals(CredentialModel.PASSWORD)) return false;
		logger.debug(" credential ");
		return true;
	}
	
	
	//CredentialInputValidator
	@Override
	public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
		// TODO Auto-generated method stub
		logger.debug(" configuracion ");
		logger.debug(" configuracion es " +credentialType);
		if (!credentialType.equals(CredentialModel.PASSWORD)) return false;
		logger.debug(" configuracion ");
		return true;
	}

	@Override
	public boolean isValid(RealmModel realm, UserModel user, CredentialInput credentialInput) 
	{
		logger.debug("inicio de la validacion del password 1 ");
		if (!supportsCredentialType(credentialInput.getType()) 
		|| !(credentialInput instanceof UserCredentialModel)) 
			return false;
		 UserCredentialModel cred = (UserCredentialModel)credentialInput;
		 logger.debug("inicio de la validacion del password 2");
 		 //if (null ==this.loadedUsers.get(user.getUsername())) return false;
 		 String username=user.getUsername();
 		 Integer tipodoc=Character.getNumericValue(username.charAt(0));
 		 String document=username.substring(1);
 		logger.debug("el tipo doc es "+tipodoc);
 		logger.debug("el  es "+username);
 		logger.debug("el tipose documento es "+document);
 		 Usuario us=new Usuario();
 		 us.setNumerodoc(document);
 		 us.setPassword(cred.getValue());
 		 boolean out=false;
 		 if(null!=this.loadedUsers.get(user.getUsername())) 
 		 {
 			DAOUsuario daou=new DAOUsuario();
 			out=daou.validPassword(tipodoc, us);
 		 }
 		return out;
	}
	//CredentialInputValidator

	
	//UserBulkUpdateProvider
	/*
	 * @Override public void grantToAllUsers(RealmModel realm, RoleModel role) { //
	 * TODO Auto-generated method stub
	 * 
	 * }
	 */
	//UserBulkUpdateProvider

	//UserRegistrationProvider
	@Override
	public UserModel addUser(RealmModel realm, String username) 
	{
		DAOUsuario daoUsuario=new DAOUsuario();
		if(username.contains("@")) 
		{
			Usuario us=new Usuario();
			us.setEmail(username);
			daoUsuario.insertUserEmail(us);
			us=daoUsuario.findbyEmail(username);
			Rol defaultrol=StayMemory.ROLES.get(Constantes.ROLDEFAULT);
			us.usuarioaddRol(defaultrol.getId(),defaultrol);
			daoUsuario.insertRolUseer(defaultrol.getId(), us);
			UserModel usm=createAdapter(realm, us, username);
			usm.setEmail(username);
			usm.setUsername("");
			return usm;
		}else {
		logger.debug("inicio del add");
		Integer tipodoc=Character.getNumericValue(username.charAt(0));
		String numerodoc=username.substring(1);
		Usuario us=new Usuario();
		us.setEmail(Constantes.defaultmail+username+".com");
		us.setNumerodoc(numerodoc);
		Rol defaultrol=StayMemory.ROLES.get(Constantes.ROLDEFAULT);
		us.usuarioaddRol(defaultrol.getId(),defaultrol);
		logger.debug("se envia "+ us.getEmail());
		logger.debug("se envia "+us.getNumerodoc());
		daoUsuario.insertUser(tipodoc,us);
		us=daoUsuario.findbyDocNumero(tipodoc,numerodoc);
		daoUsuario.insertRolUseer(defaultrol.getId(), us);
		return createAdapter(realm, us, username);
		}
	}

	@Override
	public boolean removeUser(RealmModel realm, UserModel user) 
	{
		UserModel uss=this.loadedUsers.get(user.getUsername());
		boolean out=false;
		if(null != uss) 
		{
			this.loadedUsers.remove(user.getUsername());
			out=true;
		
		DAOUsuario dao=new DAOUsuario();
		String username=user.getUsername();
		Integer tipodoc=Character.getNumericValue(username.charAt(0));
		String docuemnt=username.substring(1);
		Usuario us=new Usuario();
		us.setNumerodoc(docuemnt);		
		Integer salid=dao.deleteLogicalUpdate(tipodoc,us);
		out=salid==-1?false:true;
		}
		return out;
	}
	//UserRegistrationProvider

	
	///UserQueryProvider
	
	@Override
	public int getUsersCount(RealmModel realm) 
	{
		logger.debug("inicio para importar los usuarios ");
		if(null !=this.loadedUsers && !this.loadedUsers.isEmpty()) 
		{
			return this.loadedUsers.size();
		}
		DAOUsuario usf=new DAOUsuario();
		this.loadedUsers = createModelHash(realm, usf.findallUser());
		return this.loadedUsers.size(); 
	}
	
	
	
		
	@Override
	public List<UserModel> getUsers(RealmModel realm) 
	{
		logger.debug("inicio para importar los usuarios ");
		return getUsers(realm,0,Integer.MAX_VALUE);
	}

	@Override
	public List<UserModel> getUsers(RealmModel realm, int firstResult, int maxResults) 
	{
		logger.debug("inicio para importar los usuarios ");
		if(null !=this.loadedUsers && !this.loadedUsers.isEmpty()) 
		{
			java.util.List<UserModel> models=new java.util.ArrayList<>();
			Set<String> keys=this.loadedUsers.keySet();
			for(String key:keys) 
			{
				models.add(this.loadedUsers.get(key));	
			}
			return models;
		}else
		{
				DAOUsuario usf=new DAOUsuario();
				this.loadedUsers = createModelHash(realm, usf.findallUser());
				return convertHashToList(realm, this.loadedUsers); 
		}
	}

	@Override
	public List<UserModel> searchForUser(String search, RealmModel realm) {
		return searchForUser(search, realm, 0, Integer.MAX_VALUE);
	}

	@Override
	public List<UserModel> searchForUser(final String search, RealmModel realm, int firstResult, int maxResults) 
	{
		logger.debug("inicio de busqueda de usuarios");
		if(null !=this.loadedUsers && !this.loadedUsers.isEmpty()) 
		{
			List<UserModel> users=convertHashToList(realm,this.loadedUsers);
			UserModel[] uss=users.stream().filter
		( user -> user.getUsername().contains(search) || user.getEmail().contains(search)).
		toArray(UserModel[]::new);
			users=new ArrayList<>();
			for(UserModel us:uss) 
			{
				users.add(us);
			}
			return users;
		}else 
			{
			
				DAOUsuario usf=new DAOUsuario();
				this.loadedUsers = createModelHash(realm, usf.findallUser());
				return  convertHashToList(realm, this.loadedUsers);
			}
	}

	@Override
	public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm) {
		logger.debug("inicio de busqueda de usuarios cn parametros");
		return searchForUser(params, realm, 0, Integer.MAX_VALUE);
	}

	@Override
	public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm, int firstResult,
			int maxResults) 
	{
		logger.debug("inicio de busqueda de usuarios cn parametros con max result");
		logger.debug("valor max result" +maxResults);
		if (null == params || params.isEmpty()) { return getUsers(realm);}
		else 
		{
				if(null!=params.get("username")) 
				{
					return searchForUser(params.get("username"), realm);
				}else 
				{
					return getUsers(realm);
				}
		}
	}

	@Override
	public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group) {
		// TODO Auto-generated method stub
		return   Collections.EMPTY_LIST;
	}

	@Override
	public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults) {
		// TODO Auto-generated method stub
		return   Collections.EMPTY_LIST;
	}

	@Override
	public List<UserModel> searchForUserByUserAttribute(String attrName, String attrValue, RealmModel realm) {
		// TODO Auto-generated method stub
		return   Collections.EMPTY_LIST;
	}

	public ImpUsuarioProvider() {
		// TODO Auto-generated constructor stub
	}
	///UserQueryProvider
	
	
	
	//UserLookupProvider
	@Override
	public UserModel getUserById(String id, RealmModel realm) {
		logger.debug("Inicia mos a buscar por ID");
		StorageId storageId = new StorageId(id);
        String username = storageId.getExternalId();
        return getUserByUsername(username, realm);
	}

	@Override
	public UserModel getUserByUsername(String username, RealmModel realm) 
	{
		logger.debug("Iniciamos a buscar por  username ");
		UserModel adapter= this.loadedUsers.get(username);
		if(null!=adapter) 
		{
			logger.debug(" ya existo ");
			return adapter;
		}
		logger.debug(" lo buscamos en base de datos  ");
		DAOUsuario daou=new DAOUsuario();
		Integer tipodoc=Character.getNumericValue(username.charAt(0));
		String numeroDoc=username.substring(1);
		Usuario uss=daou.findbyDocNumero(tipodoc,numeroDoc);
		if(null!=uss) 
		{
			adapter=createAdapter(realm,uss,uss.getName());
			this.loadedUsers.put(uss.getName(),adapter);			
		}
		return adapter;
	}

	
	
	@Override
	public UserModel getUserByEmail(String email, RealmModel realm) 
	{
		logger.debug(" inicio de la consutla por email  ");
		UserModel adapter=null;
		DAOUsuario daou=new DAOUsuario();
		Usuario uss=daou.findbyEmail(email);
		if(null!=uss) 
		{
			adapter=createAdapter(realm,uss,uss.getName());
			this.loadedUsers.put(uss.getName(),adapter);			
		}
		return adapter;
	}
	//UserLookupProvider
	
	
	protected UserModel createAdapter(RealmModel real,final Usuario us,final String username)
	{
		return new ModelLocal(session, real, model, us); //debe adicionar los roles como atributos
	}
	
	
		
		
	
	
	public List<UserModel> createModelList(RealmModel mreal ,List<Usuario> usuarios) 
	{
		List<UserModel> userm=new ArrayList<>();
		for(Usuario us: usuarios) 
		{
		 UserModel usm=createAdapter(mreal, us,us.getName());
		 userm.add(usm);
		}
		return userm;
	}
	
	public List< UserModel>  convertHashToList(RealmModel mreal, java.util.Map<String,UserModel> usuarios) 
	{
		Set<String> keys=usuarios.keySet();
		List<UserModel> userm=new ArrayList<>(); 
		for(String key:keys) 
		{
			UserModel uss=usuarios.get(key);
			userm.add(uss);
		}
		return userm;
	}
	
	
	public Map<String, UserModel>  createModelHash(RealmModel mreal, java.util.Hashtable<String,Usuario> usuarios) 
	{
		Set<String> keys=usuarios.keySet();
		Map<String, UserModel> loadedUsersl=new Hashtable<String,UserModel>(); 
		for(String key:keys) 
		{
			Usuario uss=usuarios.get(key);
			UserModel usm=createAdapter(mreal, uss,uss.getName());
			loadedUsersl.put(uss.getName(), usm);
		}
		return loadedUsersl;
	}
  
}
