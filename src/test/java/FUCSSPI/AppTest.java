package FUCSSPI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;

import org.junit.Test;

import fucsspi.dao.Conexion;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testConection()
    {
    	//CO-PF2282KQ:1433
    	StringBuilder a=new StringBuilder
    	("jdbc:sqlserver://127.0.0.1:1433\\SQLEXPRESS;databaseName=FUCS;instance=SQLEXPRESS");
    	System.out.println(a.toString());
    	Connection con = Conexion.getConnection
    	(a.toString(), 
    	"pasante1","sebastian.1", "FUCS", "1433", "127.0.0.1");
    	assertTrue(con != null);
    }
    
    @Test
    public void testSepareString() 
    {
    	String name="11010230671";
    	Integer tipodoc=Character.getNumericValue(name.charAt(0));
		String numeroDoc=name.substring(1);
    	System.out.println(tipodoc);
    	System.out.println(numeroDoc);
    }
}
